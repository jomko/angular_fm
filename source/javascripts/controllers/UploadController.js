app.controller('UploadCtrl', function($scope, $http, ngDialog) {

	$scope.inputChange = function() {
		//console.log("value changed "+ $scope.json.url);

		$http.get($scope.json.url).
		success(function(data, status, headers, config) {
			
			// //add a parent attribute to each items
			var setParent = function(o) {
				for (n in o.items) {
					o.items[n].parent = o;
					setParent(o.items[n]);
				}
			}
			setParent(data);

			$scope.items = data.items;
			//console.log($scope.items);

		}).
		error(function(data, status, headers, config) {
			// log error
			console.log('error');
		});
	};

	$scope.levelDown = function(i) {
		if (i.items) {
			$scope.items = i.items;
			$scope.sublevel = true;
		} else {
			ngDialog.open({
				template: '<img src="http://static.panoramio.com/photos/large/' + i.name + '" alt="">',
				plain: true
			});
		}
	};

	$scope.levelUp = function(i) {
		$scope.items = i[0].parent.parent.items;	
		if (!$scope.items[0].parent.parent) {
			$scope.sublevel = false;
		}
	};

});









// app.directive('collection', function() {
// 	return {
// 		restrict: "E",
// 		replace: true,
// 		scope: {
// 			collection: '='
// 		},
// 		template: "<ul class='no-bullet'><member ng-repeat='member in collection' member='member'></member></ul>"
// 	}
// });

// app.directive('member', function($compile, ngDialog) {

// 	return {
// 		restrict: "E",
// 		replace: true,
// 		scope: {
// 			member: '='
// 		},
// 		template: "<li>{{member.name}}</li>",
// 		link: function(scope, element, attrs) {
// 			if (angular.isArray(scope.member.items)) {
// 				element.append("<collection collection='member.items'></collection>");
// 				// element.replaceWith($compile('<li class="folder" ng-click="LoadNew()"><ng-md-icon icon="folder_open" size="17"></ng-md-icon><span>{{member.name}}</span></li>')(scope));

// 				// scope.LoadNew = function() {
// 				// 	//console.log(element + '   v' + scope);
// 				// 	//element.append($compile("<span>here</span>")(scope));
// 				// };

// 			} else {
// 				// element.replaceWith($compile('<li class="pic" ng-click="open()"><ng-md-icon icon="image" size="17"></ng-md-icon><span>{{member.name}}</span></li>')(scope));

// 				// scope.open = function() {
// 				// 	ngDialog.open({
// 				// 		template: '<img src="http://static.panoramio.com/photos/large/' + scope.member.name + '" alt="">',
// 				// 		plain: true
// 				// 	});
// 				// }
// 			}
// 			$compile(element.contents())(scope);
// 		}
// 	}
// });